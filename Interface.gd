extends Node

signal answer_submitted

func _ready():
	var args = OS.get_cmdline_args()
	var regex = RegEx.new()
	regex.compile("--(.+)=(.+)")
	for arg in args:
		var result:RegExMatch = regex.search(arg)
		var k = result.strings[1]
		var v = result.strings[2]
		var kmethod = "arg_"+k
		if has_method(kmethod):
			call(kmethod, v)
	if not $UI/ButtonPlay.is_connected("pressed", self, "download_lesson"):
		$UI/LoadingError.text = "No first lesson specified"
		$UI/ButtonPlay.visible = false

func _process(_delta):
	if $UI/ProgressBar.visible:
		if $UI/ProgressBar.value >= 100:
			$UI/ProgressBar.visible = false
		else:
			var bodySize = $HTTP.get_body_size()
			var downloadedBytes = $HTTP.get_downloaded_bytes()
			var percent = int(downloadedBytes*100/bodySize)
			$UI/ProgressBar.value = percent

func arg_lesson(lesson_name):
	$UI/ButtonPlay.connect("pressed", self, "download_lesson", [lesson_name], CONNECT_DEFERRED)

func set_loading_error(error):
	$UI/LoadingError.visible = true
	$UI/LoadingError.text = error
	$UI/ButtonPlay.visible = true
	$UI/ButtonPlay.text = "RETRY"

func download_lesson(lesson_name:String):
	for child in $Lesson.get_children():
		$Lesson.remove_child(child)
		child.free()
	$UI/ButtonSubmitAnswer.visible = false

	# TEMP read from local json
	var filename = "res://%s.json" % lesson_name
	var file:File = File.new()
	var error = file.open(filename, File.READ)
	if error != 0:
		set_loading_error("%s open error %d" % [filename, error])
		return
	var result:JSONParseResult = JSON.parse(file.get_as_text())
	file.close()

	if result.error:
		set_loading_error("%s:%d %s" % [filename, result.error_line, result.error_string])
		return
	var lesson_data = result.result
	if !(lesson_data is Dictionary):
		set_loading_error("Expected json as object")
		return

	if load_lesson_video(lesson_name, lesson_data):
		return
	if load_lesson_scene(lesson_name, lesson_data):
		return

	var url = lesson_data.get("swfUrl")
	var regex = RegEx.new()
	regex.compile("\/([^\/]+)$")
	var regexmatch = regex.search(url)
	if not regexmatch:
		set_loading_error("Invalid download url %s" % url)
		return

	var download_file = regexmatch.strings[0]
	$HTTP.connect("request_completed", self, "_on_HTTP_request_completed", [lesson_name, lesson_data])
	$HTTP.download_file = "user://%s" % download_file
	error = $HTTP.request("https:" + url)
	if error != 0:
		set_loading_error("HTTP request error %d" % error)
		return
	$UI/ProgressBar.visible = true
	$UI/ProgressBar.value = 0

func _on_HTTP_request_completed(result: int, response_code: int,
				_headers: PoolStringArray, _body: PoolByteArray,
				lesson_name:String, lesson_data:Dictionary):
	$HTTP.disconnect("request_completed", self, "_on_HTTP_request_completed")
	var filename = $HTTP.download_file
	if result != 0:
		set_loading_error("Error %d downloading %s" % [response_code, filename])
	elif filename.ends_with(".webm"):
		load_lesson_video(lesson_name, lesson_data)
	elif filename.ends_with(".pck"):
		ProjectSettings.load_resource_pack(filename)
		load_lesson_scene(lesson_name, lesson_data)

func load_lesson_scene(lesson_name, lesson_data):
	var scene = "res://lessons/%s.tscn" % lesson_name
	if !ResourceLoader.exists(scene):
		return false

	var lesson_packedscene = load(scene)
	if not lesson_packedscene:
		set_loading_error("Can't load lesson scene "+scene)
		return false

	$UI/ButtonPlay.disconnect("pressed", self, "download_lesson")
	var lesson = lesson_packedscene.instance()
	$Lesson.add_child(lesson)
	lesson.connect("awaiting_answer", self, "await_answer")
	lesson.connect("lesson_finished", self, "lesson_finished")
	connect("answer_submitted", lesson, "evaluate_answer")
	if lesson.has_method("init_event"):
		lesson.call("init_event", lesson_name, lesson_data.eventType, lesson_data.flashVars)
	return true

func load_lesson_video(lesson_name, lesson_data):
	var video = "user://%s.webm" % lesson_name
	if !ResourceLoader.exists(video):
		video = "res://public/%s.webm" % lesson_name
		if !ResourceLoader.exists(video):
			return false

	var stream = load(video)
	if !stream:
		set_loading_error("Can't load lesson video "+video)
		return false

	$UI/ButtonPlay.disconnect("pressed", self, "download_lesson")
	$VideoPlayer.visible = true
	$VideoPlayer.stream = stream
	$VideoPlayer.connect("finished", self, "lesson_finished", [lesson_name])
	$VideoPlayer.play()
	return true

func await_answer():
	$UI/ButtonSubmitAnswer.visible = true

func _on_ButtonSubmitAnswer_pressed():
	$UI/ButtonSubmitAnswer.visible = false
	emit_signal("answer_submitted")

const NEXT_LESSONS = {
	"3.5.2.1.1IntroVidA": "3.5.2.1.2PS1Q1",
	"3.5.2.1.2PS1Q1": "3.5.2.1.3PS1Q2",
	"3.5.2.1.3PS1Q2": "3.5.2.1.4IntroVidB",
	"3.5.2.1.4IntroVidB": "3.5.2.1.5"
}

func lesson_finished(lessonname):
	$VideoPlayer.visible = false
	if $VideoPlayer.is_connected("finished", self, "lesson_finished"):
		$VideoPlayer.disconnect("finished", self, "lesson_finished")
	if NEXT_LESSONS.has(lessonname):
		$UI/ButtonPlay.connect("pressed", self, "download_lesson", [NEXT_LESSONS[lessonname]], CONNECT_DEFERRED)
		call_deferred("download_lesson", NEXT_LESSONS[lessonname])

func _notification(what):
	if $VideoPlayer and $VideoPlayer.is_playing() or $Lesson and $Lesson.get_child_count() > 0:
		if what == NOTIFICATION_WM_FOCUS_OUT:
			$VideoPlayer.paused = true
			get_tree().paused = true
			$UI/PauseDim.visible = true
		elif what == NOTIFICATION_WM_FOCUS_IN:
			$UI/ButtonPlay.text = "RESUME"
			$UI/ButtonPlay.visible = true

func _on_ButtonPlay_pressed():
	$UI/LoadingError.visible = false
	$UI/ButtonPlay.visible = false
	get_tree().paused = false
	$UI/PauseDim.visible = false
	$VideoPlayer.paused = false
