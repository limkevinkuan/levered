extends Node

var answer = -1
var correct_answer = 1
var event_name:String
var event_type:String
var event_params:Dictionary

signal awaiting_answer
signal lesson_finished

func init_event(eventname:String, eventtype:String, eventparams:Dictionary):
	event_name = eventname
	event_type = eventtype
	event_params = eventparams
	var question = eventparams.get("question")
	var answers = eventparams.get("answers")
	var error = eventparams.get("error")

	if error:
		for i in range(0, 4):
			if error[i] == "0":
				correct_answer = i
				break

	$Timer.wait_time = eventparams.get("inactivityMS", 60000) / 1000
	if $AnimationPlayer.has_animation(eventtype):
		$AnimationPlayer.play(eventtype)

	if question:
		$MultipleChoice.set_question(question)

	if answers:
		$MultipleChoice.set_answers(answers)

func _on_AnimationPlayer_animation_finished(anim_name:String):
	var method = "animation_finished_"+anim_name
	if has_method(method):
		call(method)

func animation_finished_video():
	emit_signal("lesson_finished", event_name)

func animation_finished_closed_question():
	emit_signal("awaiting_answer")

func animation_finished_no_answer():
	emit_signal("awaiting_answer")

func animation_finished_inactivity():
	emit_signal("awaiting_answer")

func animation_finished_correct():
	if event_params.no_retry:
		emit_signal("lesson_finished", event_name)

func animation_finished_incorrect1():
	if event_params.no_retry:
		emit_signal("lesson_finished", event_name)

func animation_finished_incorrect2():
	if event_params.no_retry:
		emit_signal("lesson_finished", event_name)

func animation_finished_incorrect3():
	if event_params.no_retry:
		emit_signal("lesson_finished", event_name)

func evaluate_answer():
	if answer < 0:
		$AnimationPlayer.play("no_answer")
	else:
		$MultipleChoice.show_correct(answer, correct_answer)
		if answer == correct_answer:
			$AnimationPlayer.play("correct")
		else:
			var error = event_params.get("error")[answer]
			$AnimationPlayer.play("incorrect%s" % error)

func _on_Timer_timeout():
	if $AnimationPlayer.has_animation("inactivity"):
		$AnimationPlayer.play("inactivity")

func _on_MultipleChoice_choice_pressed(i:int):
	answer = i
