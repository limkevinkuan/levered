extends Node2D

export var pathfollow2d_path:NodePath setget set_pathfollow2dpath
var pathfollow2d:PathFollow2D

func set_pathfollow2dpath(value):
	var parent = get_parent()
	if parent:
		pathfollow2d = parent.get_node(value)
		return value
	return null

func _process(_delta):
	if pathfollow2d:
		position = pathfollow2d.position
