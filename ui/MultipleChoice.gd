extends Control

signal choice_pressed

func set_question(question):
	$Question.text = question

func set_answers(answers):
	for i in range(0, 4):
		var choice:Button = $Choices.get_node(str(i))
		choice.text = answers[i]
		choice.clear_mark()

func _on_Choice_pressed(button_i:int):
	emit_signal("choice_pressed", button_i)
	for i in range(0, 4):
		var choice:Button = $Choices.get_node(str(i))
		choice.pressed = i == button_i

func show_correct(selected:int, correct:int):
	var correctchoice:Button = $Choices.get_node(str(correct))
	correctchoice.set_mark(true)

	if selected != correct:
		var selectedchoice:Button = $Choices.get_node(str(selected))
		selectedchoice.set_mark(false)
