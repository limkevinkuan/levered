extends Button

func set_mark(correct):
	$Correct.visible = correct
	$Incorrect.visible = !correct

func clear_mark():
	$Correct.visible = false
	$Incorrect.visible = false
